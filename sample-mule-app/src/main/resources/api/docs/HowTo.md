# **How To?**
---

### **How To Use The API?**
- First of all, we need to be familiarize and follow the MuleSoft API Best Practices. There are patterns to follow on invoking the API and the correct method to be used. Not all method will be used for **SkillSoft API**.

  - How do I get profile fields?
    - Use the **GET** method.
    - Provide the client-id (with the header `xh-client-id`) and client-secret (with the header `xh-client-secret`)
    - All Requests sent to the API should be in JSON format. `content-type` header should be set to **application\json**
    - No request Parameters are required
