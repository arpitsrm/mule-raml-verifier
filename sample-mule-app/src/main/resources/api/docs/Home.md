# **Workday SkillSoft Experience API**

## **Overview**
 - Workday SkillSoft Process API is used to get Human Resources specific data from Workday.
 
## **Prerequisites**
- For version v1.0 :
  - Communicate using **HTTP**
  - Must use **`application/json`** for all the payload content-type
  - Should have xh-client-id, xh-client-secret, xh-transaction-id.
  

## **Payload requirements**
- All Field names specified in JSON requests and responses
- Human Resources Process API will use the REST architecture style
- Human Resources Process API must be able to accept at least 200 records per transaction, but must be - run in real time..

## **Header Requirements**
- The following headers must be defined on each request:
  - `xh-client-id`
  - `xh-client-secret`
  - `Content-Type`
  - `xh-environment`
  