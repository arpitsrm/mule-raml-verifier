# Apisero - Raml Verifier Maven Plugin
----

### History

|	Date	      |	Author	        |	Remarks	|
|	---------   |	--------------	|	------------------	|
| 2020-31-07  | Arpit Singh | Raml Verifier Maven Plugin V1.0 |

## Introduction
- Can be used to Verify that the API Spec(Raml) in the code is in sync with the API Spec published in the Exchange
- Should be included as a maven plugin in ```Mule App pom.xml```
- Will be triggered at the ```mvn test``` stage
- Can be configured to either fail the build or show warnings, if the raml verification fails

### Using the Plugin

Should be included as a maven plugin in the project pom.xml

### Plugin Inclusion
Include the below plugin in the pom.xml of the project
```
		 <plugins>
        <plugin>
            <groupId>com.apisero</groupId>
            <artifactId>verify-raml-maven-plugin</artifactId>
            <version>0.0.1</version>
            <executions>
                <execution>
                    <goals>
                        <goal>verify-raml</goal>
                    </goals>
                </execution>
            </executions>
            <configuration>
                <scope>test</scope>
                <password>YOUR_ANYPOINT_PASSWORD</password>
                <userName>YOUR_ANYPOINT_USERNAME</userName>
                <failOnError>true/false</failOnError>
            </configuration>
        </plugin>
    </plugins>
```

### Plugin Configurations
- The plugin can be configured to run at the build or test stage using ```<scope>test</scope>```
- Enter your Anypoint credentials for the plugin to access your anypoint exchange API's
- ```<failOnError>``` is an optional paramter that can be used to configure if you want the build to fail on validation errors or no. It is ```false``` by default

### Behind the Scene Logic
- Plgun can get executed by using ```mvn install``` or ```mvn test command```
- The plugin will first get an access token using anypoint Graph API 
- It will then download and unzip the raml specification from exchange.
- Will Compare the raml from exchange with the raml present in the code base.
- The API Specification comparision is byte by byte recursive by size. More checks can be added as well. **TODO**

## Making Changes in the Plugin

### Prerequisites

```
Maven
JDK 1.8.x or later
Eclipse IDE
```

### Installing

A step by step series of examples that tell you have to get a development env running

###### Step 1: Check out the code from GitHub:
```
TODO
```

###### Step 2: Import the project to Eclipse

```
<git-checkout-path-location>/
```


## Running the tests

Not Required

## Deployment

Since this is a java maven dependency, any new changes will have to be added as a new version in the artificatory

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

We use [GitLab](http://gitlab.com/) for versioning. For the versions available, see the [tags on this repository example-readme](https://github.com/interface360/example-readme.git).
* GitHub URL: https://gitlab.com/arpitsrm/mule-raml-verifier


### FAQ

#### How are squashed pull request matched?

## Authors

* **Arpit Singh** - *AS*
