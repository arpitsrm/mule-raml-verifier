package com.apisero.plugin.util;

import static com.apisero.plugin.util.Constants.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.FileUtils;

import com.apisero.plugin.util.AccessToken;
import com.apisero.plugin.util.Constants.HeaderName;
import com.apisero.plugin.util.Constants.RequestMethod;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ExchangeOperationsUtil {

	public static HttpsURLConnection getAnypointGraphResponse(String orgId, String accessToken, String assetId, String version) throws IOException {
		String jsonInputString = getRamlRequestString(orgId, assetId, version);

		HttpsURLConnection con = (HttpsURLConnection) HttpConnectionUtil.getUrlConnection(accessToken);

		try (OutputStream os = con.getOutputStream()) {
			byte[] input = jsonInputString.getBytes("utf-8");
			os.write(input, 0, input.length);
		}

		int code = con.getResponseCode();
		// TODO throw exception if code is not 200
		if (code != 200) {
			throw new IOException("Unable to get a successfult response from ANypoint Graph API with code: " + code
					+ " and exception is: /n " + con.getResponseMessage());
		}
		return con;
	}

	public static String getAccessToken(String userName, String password) throws IOException {
		URL url = new URL(EXCHANGE_LOGIN_URL);
		StringBuilder response = new StringBuilder();
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod(RequestMethod.POST.name());

		con.setRequestProperty(HeaderName.CONTENT_TYPE.getHeaderName(), "application/json");
		con.setRequestProperty(HeaderName.ACCEPT.getHeaderName(), "*/*");

		con.setDoOutput(true);

		String jsonInputString = getTokenRequestString(userName, password);

		try (OutputStream os = con.getOutputStream()) {
			byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
			os.write(input, 0, input.length);
		}

		int code = con.getResponseCode();

		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
		}

		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		AccessToken accessToken = gson.fromJson(response.toString(), AccessToken.class);

		return accessToken.getAccessToken();
	}

	private static String getTokenRequestString(String userName, String password) {
		String jsonInputString = "{\"username\": \"" + userName + "\", \"password\": \"" + password + "\"}";
		return jsonInputString;
	}

	private static String getRamlRequestString(String organizationId,String assetId, String version) {
		String jsonInputString = "";
		if (organizationId.isEmpty()) {
			jsonInputString = "{\"query\":\"{assets(query:{searchTerm: \\\""+assetId+"\\\", type: \\\"rest-api\\\"}, " + version + ": true){groupId,assetId,version,description,name,type, files {classifier packaging externalLink md5 }}}\"}";
		} else {
			jsonInputString = "{\"query\":\"{assets(query:{searchTerm: \\\""+assetId+"\\\", type: \\\"rest-api\\\", organizationIds: \\\""
					+ organizationId
					+ "\\\"}, latestVersionsOnly: true){assetId, files {groupId,assetId,version,description,name,type, files {classifier packaging externalLink md5 } }}}\"}";
		}

		return jsonInputString;
	}

}
