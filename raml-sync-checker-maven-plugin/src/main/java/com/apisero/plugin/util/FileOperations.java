package com.apisero.plugin.util;

import static com.apisero.plugin.util.Constants.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import com.apisero.plugin.util.Constants.FileExtension;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class FileOperations {

	public static Iterator<File> getAllFilesWithExtension(String rootDir, String extension) {
		return FileUtils.iterateFiles(new File(rootDir), FileFilterUtils.suffixFileFilter(extension),
				TrueFileFilter.INSTANCE);
	}
	
	public static Iterator<File> getAllFilesWithExtension1(String rootDir, String extension) {
		return FileUtils.iterateFiles(new File(rootDir), FileFilterUtils.suffixFileFilter(extension),
				null);
	}

	public static File getRootRaml(String rootDir, String extension) {
		Iterator<File> ramlFiles = FileOperations.getAllFilesWithExtension(rootDir, extension);
		File ramlFile = ramlFiles.hasNext() ? ramlFiles.next() : null;
		return ramlFile;
	}

	public static void deleteFile(String filePath) throws IOException {
		FileUtils.forceDelete(new File(filePath));
	}

	public static void writeToFile(String filePath, String data, Charset utf8) throws IOException {
		FileUtils.write(new File(filePath), data, StandardCharsets.UTF_8);
	}

	public static String getFileDataAsString(HttpsURLConnection con) throws UnsupportedEncodingException, IOException {
		StringBuilder response = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			// System.out.println(response.toString());
		}
		return response.toString();
	}

	public static void unzipRamls(String DownloadPath, String RamlPath) throws IOException {
		new UnzipUtility().unzipAllInDirectory(DownloadPath, RamlPath);
	}

	public static String downloadRamlsAsZip(String response, String localAssetId, String rootPath)
			throws MalformedURLException, IOException {
		File oldName = new File(rootPath + PATH_TEMP_FILE);
		String DownloadedAssetId = "";
		JsonObject jsonObject = new JsonParser().parse(response.toString()).getAsJsonObject();
		JsonArray assets = jsonObject.getAsJsonObject("data").getAsJsonArray("assets");
		List<String> assetsUrlList = new ArrayList<>();
		for (JsonElement jsobElement : assets) {
			String assetName = jsobElement.getAsJsonObject().get("name").getAsString();
			JsonArray files = jsobElement.getAsJsonObject().getAsJsonArray("files");
			for (JsonElement fileElement : files) {
				String classifier = fileElement.getAsJsonObject().get("classifier").getAsString();
				String packaging = fileElement.getAsJsonObject().get("packaging").getAsString();
				DownloadedAssetId = jsobElement.getAsJsonObject().get("assetId").getAsString();
				
				if (FileExtension.ZIP.getExtension().equalsIgnoreCase(packaging)
						&& FileExtension.FAT_RAML.getExtension().equalsIgnoreCase(classifier)
						&& (localAssetId.contains(assetName))) {
					String downloadLink = fileElement.getAsJsonObject().get("externalLink").getAsString();
					assetsUrlList.add(fileElement.getAsJsonObject().get("externalLink").getAsString());

					FileUtils.copyURLToFile(new URL(downloadLink), new File(rootPath + PATH_TEMP_FILE), 10000, 10000);
					File newName = new File(rootPath + "Downloads" + File.separatorChar + localAssetId + ".zip");
					FileUtils.copyFile(oldName, newName);
					System.out.println("Downloaded RAML ##### "+assetName+ " #####");

				}
			}
		}
		// FileUtils.deleteQuietly(oldName);
		return localAssetId;
	}

}
