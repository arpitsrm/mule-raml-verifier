package com.apisero.plugin.util;

import static com.apisero.plugin.util.Constants.EXCHANGE_GRAPH_URL;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import com.apisero.plugin.util.Constants.HeaderName;
import com.apisero.plugin.util.Constants.RequestMethod;

public class HttpConnectionUtil {

	public static URLConnection getUrlConnection(String accessToken) throws IOException {
		URL url = new URL(EXCHANGE_GRAPH_URL);
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod(RequestMethod.POST.name());

		con.setRequestProperty(HeaderName.AUTHORIZATION.getHeaderName(), "Bearer " + accessToken);
		con.setRequestProperty(HeaderName.CONTENT_TYPE.getHeaderName(), "application/json");
		con.setRequestProperty(HeaderName.ACCEPT.getHeaderName(), "*/*");

		con.setDoOutput(true);
		return con;
	}

	public static void closeUrlConnection(HttpsURLConnection con) {
		con.disconnect();
	}

}
