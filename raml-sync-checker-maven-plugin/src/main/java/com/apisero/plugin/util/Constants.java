package com.apisero.plugin.util;

import java.io.File;

public class Constants {

	public static final String EXCHANGE_LOGIN_URL = "https://anypoint.mulesoft.com/accounts/login";
	public static final String EXCHANGE_GRAPH_URL = "https://anypoint.mulesoft.com/graph/api/v1/graphql";
	
	public static final String PATH_TEMP_FILE = "Downloads" + File.separatorChar + "temp";
	public static final String PATH_DOWNLOADS = "Downloads";
	public static final String PATH_RAMLS = "Ramls";
	public static final String PATH_JARS = "jars";
	public static final String PATH_VALIDATION_ERRORS = "ValidationErrors";
	
	public static enum FileExtension {
		ZIP("zip"), RAML("raml"), FAT_RAML("fat-raml");

		private String extension;

		FileExtension(String extension) {
			this.extension = extension;
		}

		public String getExtension() {
			return this.extension;
		}
	};

	public static enum RequestMethod {
		POST, GET
	};

	public static enum HeaderName {
		CONTENT_TYPE("Content-Type"), AUTHORIZATION("Authorization"), ACCEPT("Accept");

		private String headerName;

		HeaderName(String headerName) {
			this.headerName = headerName;
		}

		public String getHeaderName() {
			return this.headerName;
		}
	};

	public static void main(String args[]) {
		System.out.println(HeaderName.CONTENT_TYPE.getHeaderName());
	}
	
}
