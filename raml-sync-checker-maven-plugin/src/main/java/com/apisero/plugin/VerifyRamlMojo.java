package com.apisero.plugin;

import static com.apisero.plugin.util.Constants.PATH_DOWNLOADS;
import static com.apisero.plugin.util.Constants.PATH_RAMLS;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import com.apisero.plugin.util.ExchangeOperationsUtil;
import com.apisero.plugin.util.FileOperations;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.flipkart.zjsonpatch.JsonDiff;

/**
 * Counts the number of maven dependencies of a project.
 * 
 * It can be filtered by scope.
 *
 */
@Mojo(name = "verify-raml", defaultPhase = LifecyclePhase.TEST)
public class VerifyRamlMojo extends AbstractMojo {
	
	private boolean isInSync = true;
	private String finalResult = "";

	/**
	 * Scope to filter the dependencies.
	 */
	@Parameter(property = "scope")
	String scope;

	@Parameter(property = "anypoint.username")
	private String userName;

	@Parameter(property = "anypoint.password")
	private String password;

	/**
	 * Optional Parameter- Directory Path where the raml comparision report is
	 * generated. Default is ${projectDir}.
	 */
	@Parameter(property = "download.directory", defaultValue = "", required = false, readonly = true)
	private String downloadDirectory;

	/**
	 * Optional Parameter- Relative Path to raml root directory from ${projectDir}.
	 * Default is src/main/resoruces/api.
	 */
	@Parameter(property = "raml.path", defaultValue = "src/main/resources/api", required = false, readonly = true)
	private String projectRamlPath;

	/**
	 * Optional Parameter- Version of Raml in Exchange. This is provided as multiple
	 * developers might be working on the same Raml but with different versions at
	 * the same time. And only one can be published in the exchange. Default: Latest
	 * version in the exchange
	 */
	@Parameter(property = "exchange.version", defaultValue = "latestVersionsOnly", required = false, readonly = true)
	private String exchangeVersion;

	/**
	 * Gives access to the Maven project information.
	 */
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;

	/**
	 * Flag to fail build if validation failure
	 */
	@Parameter(property = "failOnError", defaultValue = "false", required = false, readonly = true)
	Boolean failOnError;

	public void execute() throws MojoExecutionException, MojoFailureException {

		try {
			if (!downloadDirectory.endsWith("/"))
				downloadDirectory = downloadDirectory + File.separatorChar;
			verify();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new MojoFailureException(e.getMessage());
		}
	}

	private void verifyDirsAreEqual(Path one, Path other) throws MojoFailureException, IOException {
		try {
			/*
			 * getLog().info("\t=== comparing the downloaded api spec present in " +
			 * one.toString() + " with the api spec directory present in project " + other +
			 * " === ");
			 */
			
			Files.walkFileTree(one, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					FileVisitResult result = super.visitFile(file, attrs);
					// get the relative file name from path "one"
					Path relativize = one.relativize(file);
					// construct the path for the counterpart file in "other"
					Path fileInOther = other.resolve(relativize);
					if (!fileInOther.getFileName().toString().contains("exchange")) {
						
						
						ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
				        JsonNode file1 = objectMapper.readTree(fileInOther.toFile());
				        JsonNode file2 = objectMapper.readTree(file.toFile());
				        JsonNode patch = JsonDiff.asJson(file1, file2);
				        if(patch.isArray() && patch.size() > 0) {
				        	isInSync = false;
				        	finalResult += "\n" + objectMapper.writeValueAsString(patch);
				        }
						
					} else {
						getLog().info("\tSkipping exchange.json\n\n");
					}
					return result;
				}
			});
			if(isInSync) {
				getLog().info("\tRAMLS IN SYNC!!!. Continuing with the build\n\n");
			}
			else {
				getLog().info("\t##################################################");
	        	getLog().info("\tRamls not in sync. Difference Details");
	        	getLog().info(finalResult);
	        	getLog().info("\t##################################################");
	        	throw new MojoFailureException("RAMLS Are Not in sync. Please see the report ");
			//	getLog().info("\tRAMLS NOT IN SYNC!!!. Continuing with the build");
			}
		} catch (Exception e) {
			throw new MojoFailureException("RAMLS Are Not in sync. Please see the report ");
		} 

	}

	public void verify() throws InterruptedException, ExecutionException, IOException, MojoFailureException {
		try {
			downloadDirectory = downloadDirectory == null || downloadDirectory.isEmpty() ? "" : downloadDirectory;
			String ramlDirPath = Paths.get(projectRamlPath).toString();
			File ramlFile = FileOperations.getAllFilesWithExtension1(ramlDirPath, "raml").next();
			ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
	        JsonNode ramlFileJson = objectMapper.readTree(ramlFile);
	        String title = ramlFileJson.get("title").asText();
	        
			/*
			 * WebApiDocument model = (WebApiDocument) Raml10.parse("file://" +
			 * ramlFile.getPath()).get(); WebApi api = (WebApi) model.encodes();
			 */
	        getLog().info("\tRAML Title: " + title);

			/*
			 * Create Downloads folder to download all assets zip files
			 */
			FileOperations.writeToFile(downloadDirectory + "Downloads" + File.separatorChar + "temp", "",
					StandardCharsets.UTF_8);

			/*
			 * Create Ramls folder to unzip all downloaded asset zip files
			 */
			FileOperations.writeToFile(downloadDirectory + "Ramls" + File.separatorChar + "temp", "",
					StandardCharsets.UTF_8);

			/*
			 * Get Exchange API access token
			 */
			getLog().info("\tAuthenticating Anypoint with the provided credentials");
			String accessToken = ExchangeOperationsUtil.getAccessToken(userName, password);
			
			getLog().info("\tAuthentication Successfull!\n\n");
			
			Thread.sleep(2000);
			/*
			 * Get Graph API response to get assets download/external link from Anypoint
			 * Graph API
			 */
			getLog().info("\tGetting " + ramlFile.getName() + " Raml download link from Exchange with provided Version: "+ exchangeVersion);
			HttpsURLConnection con = ExchangeOperationsUtil.getAnypointGraphResponse("", accessToken,
					title, exchangeVersion);
			getLog().info("\tDownload Link Received\n\n");
			
			String response = FileOperations.getFileDataAsString(con);
			
			Thread.sleep(2000);
			
			
			
			/*
			 * Download all ramls as zip and save to /Downloads
			 */
			
			getLog().info("\tDownloading " + ramlFile.getName() + " Raml from provided Link");
			FileOperations.downloadRamlsAsZip(response, title, downloadDirectory);
			getLog().info("\tDownload Successfull\n\n");
			
			Thread.sleep(2000);
			
			

			/*
			 * Unzip all zipped ramls and save to /Ramls
			 */
			
			getLog().info("\tExtracting the downloaded Raml Folder");
			FileOperations.unzipRamls(downloadDirectory + PATH_DOWNLOADS,
					downloadDirectory + PATH_RAMLS + File.separatorChar);
			getLog().info("\tFolder Extracted Successfully\n\n");
			
			Thread.sleep(2000);
			
			

			/*
			 * Delete temp directory
			 */
			FileOperations.deleteFile(downloadDirectory + "Ramls" + File.separatorChar + "temp");
			
			getLog().info("\t######### STARTING FILE SYNC CHECK ##########");
			Thread.sleep(2000);
			verifyDirsAreEqual(Paths.get(downloadDirectory + PATH_RAMLS + File.separatorChar + title),
					Paths.get(ramlDirPath));
			getLog().info("\t######### COMPLETED FILE SYNC CHECK ##########\n\n");
			
			
			
			
		} finally {
			getLog().info("\tDeleting the temp folder ## "+downloadDirectory + PATH_RAMLS+ " ### "+downloadDirectory + PATH_DOWNLOADS);
			FileOperations.deleteFile(downloadDirectory + PATH_RAMLS);
			FileOperations.deleteFile(downloadDirectory + PATH_DOWNLOADS);
			getLog().info("\t######### DELETED THE TEMP FOLDERS FROM " + PATH_RAMLS + " AND " + PATH_DOWNLOADS + "##########");
		}

	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDownloadDirectory() {
		return downloadDirectory;
	}

	public void setDownloadDirectory(String downloadDirectory) {
		this.downloadDirectory = downloadDirectory;
	}

	public MavenProject getProject() {
		return project;
	}

	public void setProject(MavenProject project) {
		this.project = project;
	}

	public Boolean getFailOnError() {
		return failOnError;
	}

	public void setFailOnError(Boolean failOnError) {
		this.failOnError = failOnError;
	}

	public String getProjectRamlPath() {
		return projectRamlPath;
	}

	public void setProjectRamlPath(String projectRamlPath) {
		this.projectRamlPath = projectRamlPath;
	}

	public String getExchangeVersion() {
		return exchangeVersion;
	}

	public void setExchangeVersion(String exchangeVersion) {
		this.exchangeVersion = exchangeVersion;
	}

}
